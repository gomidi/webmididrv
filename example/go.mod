module gitlab.com/gomidi/webmididrv/example

go 1.14

require gitlab.com/gomidi/midi v1.23.0
require gitlab.com/gomidi/webmididrv v0.0.1

replace gitlab.com/gomidi/webmididrv => ../